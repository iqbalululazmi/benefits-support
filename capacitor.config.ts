import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.benefitsadmin.app',
  appName: 'benefits-support',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
