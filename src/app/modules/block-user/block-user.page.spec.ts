import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlockUserPage } from './block-user.page';

describe('BlockUserPage', () => {
  let component: BlockUserPage;
  let fixture: ComponentFixture<BlockUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlockUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
