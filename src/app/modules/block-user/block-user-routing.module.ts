import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlockUserPage } from './block-user.page';

const routes: Routes = [
  {
    path: '',
    component: BlockUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlockUserPageRoutingModule {}
