import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InternalPlanPage } from './internal-plan.page';

const routes: Routes = [
  {
    path: '',
    component: InternalPlanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InternalPlanPageRoutingModule {}
