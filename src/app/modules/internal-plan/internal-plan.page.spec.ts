import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternalPlanPage } from './internal-plan.page';

describe('InternalPlanPage', () => {
  let component: InternalPlanPage;
  let fixture: ComponentFixture<InternalPlanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalPlanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternalPlanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
