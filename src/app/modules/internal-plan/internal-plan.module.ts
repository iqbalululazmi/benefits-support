import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternalPlanPageRoutingModule } from './internal-plan-routing.module';

import { InternalPlanPage } from './internal-plan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InternalPlanPageRoutingModule
  ],
  declarations: [InternalPlanPage]
})
export class InternalPlanPageModule {}
