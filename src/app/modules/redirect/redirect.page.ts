import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AuthData, LoginResponse } from '@dto/auth.dto';
import { RequestAPI } from '@dto/request.dto';
import { AuthApiService } from '@remotes/auth/auth-api.service';
import { BaseUrlService } from '@remotes/base-url/base-url.service';
import { GreatdayApi } from '@remotes/greatday.api';
import { TokenService } from '@remotes/token/token.service';
import { AlertService } from '@services/alert/alert.service';
import { NavigationService } from '@services/navigation/navigation.service';
import { AuthStorageService } from '@storage/auth-storage.service';
import { BaseStorageService } from '@storage/base-storage.service';
import { CommonStorageService } from '@storage/common-storage.service';
import { AnimationItem } from 'lottie-web';
import { AnimationOptions } from 'ngx-lottie';
import { RouteEnum } from 'src/app/core/constants/routes/routes';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.page.html',
  styleUrls: ['./redirect.page.scss'],
})
export class RedirectPage implements OnInit {
  returnUrl = 'home';
  spinner: any;
  authSubscription: any;
  options: AnimationOptions = {
    path: 'assets/json/benefit-logo.json',
  };

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private authApiService: AuthApiService,
    private authStorageService: AuthStorageService,
    private navigationService: NavigationService,
    private commonStorageService: CommonStorageService,
    private tokenService: TokenService,
    private baseUrlService: BaseUrlService,
    private baseStorageService: BaseStorageService,
    private greatdayApi: GreatdayApi,
    private alertService: AlertService,
  ) {}

  async ngOnInit() {
    try {
      await this.waitFor(3000);
      if (this.route.snapshot.queryParams) {
        this.returnUrl = this.route.snapshot.queryParams.returnUrl;
        const parsedAuthData = await this.checkingAuth();
        if (parsedAuthData?.data) {
          const returnUrl = parsedAuthData.data.route;
          const navigationExtras: NavigationExtras = {
            replaceUrl: true,
            state: {
              data: parsedAuthData.data,
            },
          };
          this.router.navigateByUrl(returnUrl, navigationExtras);
        } else {
          this.router.navigateByUrl(this.returnUrl, { replaceUrl: true });
        }
      } else {
        this.navigationService.navigateForward('login');
      }
    } catch (e) {
      console.log(e);
      this.navigationService.mainGreatdayPage();
    }
  }

  async checkingAuth() {
    try {
      const params = this.route.snapshot.params;
      if (params.id) {
        const authData = params.id;
        const parsedAuthData: AuthData = JSON.parse(atob(authData));
        parsedAuthData.data.route = RouteEnum.HOME;
        this.initialServices(parsedAuthData);
        await this.synchronizeTokenMarketplace();
        const loginData = await this.authApiService.loginData().toPromise();
        this.setGreatdayStorage(loginData, parsedAuthData);
        return parsedAuthData;
      } else {
        const parsedAuthData: any = {};
        return parsedAuthData;
      }
    } catch (error) {
      console.log(error);
      this.navigationService.mainGreatdayPage();
    }
  }

  initialServices(parsedAuthData: AuthData) {
    this.baseStorageService.clearGreatDayStorage();
    this.commonStorageService.setAppFrom({ appFrom: 'greatday', userAgent: null });
    this.baseUrlService.auth = {
      ist: { GOPATHNEW: parsedAuthData.storedPrelogin?.GOPATHNEW, GOPATH: parsedAuthData.storedPrelogin.GOPATH },
    };
    this.tokenService.token = parsedAuthData.token;
  }

  async synchronizeTokenMarketplace() {
    try {
      const request: RequestAPI = {
        model: 'benefitService',
        func: 'synchronizeTokenMarketplace',
      };

      await this.greatdayApi.get(request).toPromise();
    } catch (e) {
      this.alertService.errorResponse(e);
    }
  }

  async setGreatdayStorage(loginData: LoginResponse, parsedAuthData: AuthData) {
    const SFGoAuth = await this.authStorageService.getSFGoAuth();
    if (SFGoAuth) {
      if (loginData.accId !== SFGoAuth.accId) {
        await this.commonStorageService.removeLastTransaction();
      }
    }
    await this.authStorageService.setSFGoAuth(loginData);
    await this.authStorageService.setStoredPrelogin(parsedAuthData.storedPrelogin);
  }

  private waitFor(timeout: number): Promise<void> {
    return new Promise<void>((resolve) => {
      setTimeout(() => resolve(), timeout);
    });
  }

  animationCreated(animationItem: AnimationItem): void {
    console.log(animationItem);
  }
}
