import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule, IonNav } from '@ionic/angular';
import { HomePage } from '@modules/home/home.page';
import { TranslateModule } from '@ngx-translate/core';
import { RedirectPage } from './redirect.page';

describe('RedirectPage', () => {
  let component: RedirectPage;
  let fixture: ComponentFixture<RedirectPage>;
  window.onbeforeunload = jasmine.createSpy();

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [RedirectPage],
        imports: [
          IonicModule,
          RouterTestingModule.withRoutes([{ path: 'home', component: HomePage }]),
          HttpClientTestingModule,
          TranslateModule.forRoot(),
        ],
        providers: [IonNav],
      }).compileComponents();

      fixture = TestBed.createComponent(RedirectPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    spyOn(component.router, 'navigate').and.resolveTo();
    expect(component).toBeTruthy();
  });
});
