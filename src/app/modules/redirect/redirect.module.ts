import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LottieModule } from 'ngx-lottie';
import { RedirectPageRoutingModule } from './redirect-routing.module';
import { RedirectPage } from './redirect.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RedirectPageRoutingModule, LottieModule],
  declarations: [RedirectPage],
})
export class RedirectPageModule {}
