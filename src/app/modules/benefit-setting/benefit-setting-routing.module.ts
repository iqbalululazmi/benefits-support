import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BenefitSettingPage } from './benefit-setting.page';

const routes: Routes = [
  {
    path: '',
    component: BenefitSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BenefitSettingPageRoutingModule {}
