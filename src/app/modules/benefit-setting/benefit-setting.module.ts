import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BenefitSettingPageRoutingModule } from './benefit-setting-routing.module';

import { BenefitSettingPage } from './benefit-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BenefitSettingPageRoutingModule
  ],
  declarations: [BenefitSettingPage]
})
export class BenefitSettingPageModule {}
