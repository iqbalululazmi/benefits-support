import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BenefitSettingPage } from './benefit-setting.page';

describe('BenefitSettingPage', () => {
  let component: BenefitSettingPage;
  let fixture: ComponentFixture<BenefitSettingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitSettingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BenefitSettingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
