import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, UrlMatcher, UrlMatchResult, UrlSegment } from '@angular/router';

const customRedirect: UrlMatcher = (segments: UrlSegment[]): UrlMatchResult => {
  const { length } = segments;
  const firstSegment = segments[0];
  if (firstSegment) {
    if (firstSegment.path === 'redirect' && length >= 2) {
      const idSegments = segments.slice(1);
      const idPaths = idSegments.map((segment) => segment.path);
      const mergedId = idPaths.join('/');
      const idSegment: UrlSegment = new UrlSegment(mergedId, { id: mergedId });
      return { consumed: segments, posParams: { id: idSegment } };
    }
  }
  return { consumed: [] };
};

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    matcher: customRedirect,
    loadChildren: () => import('@modules/redirect/redirect.module').then((m) => m.RedirectPageModule),
  },
  {
    path: 'home',
    loadChildren: () => import('@modules/home/home.module').then((m) => m.HomePageModule),
    data: { breadcrumb: 'Dashboard' },
  },
  {
    path: 'benefit-setting',
    loadChildren: () =>
      import('./modules/benefit-setting/benefit-setting.module').then((m) => m.BenefitSettingPageModule),
  },
  {
    path: 'internal-plan',
    loadChildren: () => import('./modules/internal-plan/internal-plan.module').then((m) => m.InternalPlanPageModule),
  },
  {
    path: 'block-user',
    loadChildren: () => import('./modules/block-user/block-user.module').then((m) => m.BlockUserPageModule),
  },
  {
    path: 'team-member',
    loadChildren: () => import('./modules/team-member/team-member.module').then((m) => m.TeamMemberPageModule),
  },
  {
    path: 'transaction-report',
    loadChildren: () =>
      import('./modules/transaction-report/transaction-report.module').then((m) => m.TransactionReportPageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      initialNavigation: 'enabled',
      paramsInheritanceStrategy: 'always',
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
