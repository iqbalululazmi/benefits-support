import { Component, ElementRef, Host, OnInit } from '@angular/core';
import { NavigationService } from '@services/navigation/navigation.service';
import { menuBenefit } from 'src/app/core/constants/menu/menu';
import { BaseMenu } from 'src/app/core/models/menu.model';

interface SidebarMenu {
  title: string;
  menus: ListMenu[];
}

interface ListMenu {
  id: number;
  label: string;
  child: BaseMenu[];
}

interface ChildMenu {
  id: number;
  activeMenu: boolean;
  subMenu: string;
  sideType: string;
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isCollapse = false;
  activeMenu = '';
  public showSideMenu = 0;
  public toggleSidemenu = true;
  public sidebarMenu: SidebarMenu = {
    title: '',
    menus: [
      {
        id: 0,
        label: 'Activity',
        child: [],
      },
      {
        id: 1,
        label: 'Management',
        child: [],
      },
      {
        id: 2,
        label: 'Report',
        child: [],
      },
      {
        id: 3,
        label: 'Administrator',
        child: [],
      },
    ],
  };
  constructor(@Host() public host: ElementRef, private navigationService: NavigationService) {}

  ngOnInit() {
    this.handleMenu();
    console.log(this.sidebarMenu);
  }

  handleMenu() {
    this.host.nativeElement.classList.add('active-sidebar');
    this.host.nativeElement.classList.remove('minimize-sidebar');
    const header = document.querySelector('app-navbar');
    if (header) {
      header.classList.add('show-breadcrumb');
    }
    this.sidebarMenu.title = 'Benefits';
    this.sidebarMenu.menus.forEach((menu) => {
      menu.child = menuBenefit.filter((item: BaseMenu, index: number) => {
        if (item.activeSubMenu) {
          const nameCapitalized = item.sideType.charAt(0).toUpperCase() + item.sideType.slice(1);
          this.activeMenu = nameCapitalized;
        }
        item.id = index;
        return item.sideType === menu.label.toLowerCase();
      });
    });
    this.toggleCollapse(true);
  }

  toggleCollapse(firstLoad?: boolean) {
    if (!firstLoad) {
      this.isCollapse = !this.isCollapse;
    }
    const sidebar = document.querySelector('app-navbar');
    if (sidebar !== null) {
      if (!this.isCollapse) {
        this.host.nativeElement.classList.add('minimize-sidebar');
        sidebar.classList.add('full-breadcrumb');
      } else {
        this.host.nativeElement.classList.remove('minimize-sidebar');
        sidebar.classList.remove('full-breadcrumb');
      }
    }
  }

  async openPage(parentId: any, childId: any, item: BaseMenu) {
    await this.getActiveMenu(parentId, childId);
    console.log(item);
    this.navigationService.navigateForward(item.route);
  }

  async getActiveMenu(parentId: any, childId: any) {
    this.sidebarMenu.menus.forEach((parent) => {
      parent.child.forEach((child) => {
        if (parent.id === parentId && child.id === childId) {
          child.activeSubMenu = true;
          const nameCapitalized = child.sideType.charAt(0).toUpperCase() + child.sideType.slice(1);
          this.activeMenu = nameCapitalized;
        } else {
          child.activeSubMenu = false;
        }
      });
    });
  }

  toggleShow(e: any) {
    const el = document.getElementById('parent' + e);
    if (el !== null) {
      if (el.classList.contains('hide')) {
        el.classList.remove('hide');
      } else {
        el.classList.add('hide');
      }
    }
  }
}
