import { Component, OnInit } from '@angular/core';
import { BreadcrumbCustomService } from '@services/lib/breadcrumb/breadcrumb-custom.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './app-breadcrumb.component.html',
  styleUrls: ['./app-breadcrumb.component.scss'],
})
export class AppBreadcrumbComponent implements OnInit {
  // @Input() data = [];
  // @Output() navigate = new EventEmitter();

  constructor(public breadcrumb: BreadcrumbCustomService) {}

  ngOnInit() {
    console.log(this.breadcrumb.getRouter())
    console.log('');
  }

  toHome() {
    // this.menuService.setToggleSidemenu();
    // this.navigationService.setHome();
  }
}
