import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GdxIconComponentModule } from '@dataon/greatday-library';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { BreadcrumbModule } from 'xng-breadcrumb';
import { AppBreadcrumbComponent } from './app-breadcrumb.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FlexLayoutModule,
    BreadcrumbModule,
    AngularSvgIconModule,
    GdxIconComponentModule,
    TranslateModule,
  ],
  exports: [AppBreadcrumbComponent],
  declarations: [AppBreadcrumbComponent],
})
export class AppBreadcrumbComponentModule {}
