import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@remotes/base/base-api.service';
import { Observable } from 'rxjs/internal/Observable';
import { LoginRequest, LoginResponse } from '@dto/auth.dto';
import { RequestBuilder, RequestData, ResponseData } from '@dto/base.dto';

@Injectable({
  providedIn: 'root',
})
export class AuthApiService {
  private model = 'auth';

  constructor(private baseApiService: BaseApiService, private httpClient: HttpClient) {}

  public loginShareAccount(data: LoginRequest) {
    const method = 'POST';
    const func = 'loginShareAccount';
    const params = data;
    const urlParams: any = {};
    const builder: RequestBuilder = { method, model: this.model, func, params, urlParams };
    const url = this.baseApiService.urlBuilder(builder);
    const request: RequestData = { method, url, params };
    return this.baseApiService.request<LoginResponse>(request);
  }

  public checkLDAP(companyCode: string): Observable<ResponseData<boolean>> {
    const method = 'GET';
    const func = 'checkLDAP';
    const urlParams: any = {
      ist: companyCode,
    };
    const builder: RequestBuilder = { method, model: this.model, func, params: {}, urlParams };
    const url = this.baseApiService.urlBuilder(builder);
    const request: RequestData = { method, url };
    return this.baseApiService.request<ResponseData<boolean>>(request);
  }

  public lifeSpanPassword(): Observable<ResponseData<boolean>> {
    const method = 'GET';
    const func = 'lifeSpanPassword';
    const params: any = {};
    const urlParams: any = {};
    const timeout = 3000;
    const builder: RequestBuilder = { method, model: this.model, func, params, urlParams };
    const url = this.baseApiService.urlBuilder(builder);
    const request: RequestData = { method, url, timeout };
    return this.baseApiService.request<ResponseData<boolean>>(request);
  }

  public logout(): Observable<ResponseData<any>> {
    const method = 'POST';
    const func = 'logout';
    const params: any = {};
    const urlParams: any = {};
    const builder: RequestBuilder = { method, model: this.model, func, params, urlParams };
    const url = this.baseApiService.urlBuilder(builder);
    const request: RequestData = { method, url, params };
    return this.baseApiService.request<ResponseData<any>>(request);
  }

  public loginData(): Observable<LoginResponse> {
    const method = 'GET';
    const func = 'loginData';
    const params: any = {};
    const urlParams: any = {};
    const builder: RequestBuilder = { method, model: this.model, func, params, urlParams };
    const url = this.baseApiService.urlBuilder(builder);
    const request: RequestData = { method, url };
    return this.baseApiService.request<LoginResponse>(request);
  }

  public refreshToken(payload: { accessToken: string; refreshToken: string }): Observable<any> {
    const method = 'POST';
    const func = 'refreshToken';
    const params: any = payload || {};
    const urlParams: any = {};
    const builder: RequestBuilder = { method, model: this.model, func, params, urlParams };
    const url = this.baseApiService.urlBuilder(builder);
    const request: RequestData = { method, url, params };
    return this.baseApiService.request<ResponseData<any>>(request);
  }

  public login(data: any): Observable<any> {
    const headers = new HttpHeaders({ timeout: '60000' }).append('Content-Type', 'application/json');
    const preLoginData = data.preLoginData;
    const goPath = preLoginData.GOPATHNEW + '/auth/login';
    return this.httpClient.post(goPath, JSON.stringify(data), { headers });
  }
}
