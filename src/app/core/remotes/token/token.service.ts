import { Injectable } from '@angular/core';
import { AuthStorageService } from '@storage/auth-storage.service';
import { CommonStorageService } from '@storage/common-storage.service';
import { AppFromEnum } from './token.constant';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  protected instanceToken = '';
  constructor(private authStorageService: AuthStorageService, private commonStorageService: CommonStorageService) {}

  set token(token: string) {
    this.instanceToken = token;
  }

  get token(): string {
    return this.instanceToken;
  }

  async useTokenService() {
    const app = await this.commonStorageService.getAppFrom();
    if (app) {
      if (app.appFrom === AppFromEnum.GREATDAY) {
        const greatdayResponse = await this.authStorageService.getSFGoAuth();
        if (greatdayResponse) {
          this.instanceToken = greatdayResponse.id;
        } else {
          this.instanceToken = '';
        }
      } else if (app.appFrom === AppFromEnum.BENEFIT) {
        const benefitResponse = await this.authStorageService.getBenefitAuth();
        if (benefitResponse) {
          this.instanceToken = benefitResponse.id;
        } else {
          this.instanceToken = '';
        }
      } else {
        this.instanceToken = '';
      }
    } else {
      this.instanceToken = '';
    }
  }

  haveToken(): boolean {
    if (this.instanceToken !== null) {
      return true;
    }

    return false;
  }
}
