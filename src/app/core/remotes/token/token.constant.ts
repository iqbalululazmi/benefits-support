export enum TokenTypeEnum {
  GDTOKEN = 'gdtoken',
  BENEFITTOKEN = 'benefittoken',
}

export enum AppFromEnum {
  GREATDAY = 'greatday',
  BENEFIT = 'benefit',
}
