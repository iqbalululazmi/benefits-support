import { Injectable } from '@angular/core';
import { RequestAPI, RequestBuilder } from '@dto/base.dto';
import { BaseApiService } from '@remotes/base/base-api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PpobApi {
  protected defaultModel = 'topupAndBill';
  protected service = 'ppob';

  constructor(private baseApi: BaseApiService) {}

  get(request: RequestAPI): Observable<any> {
    const method = 'GET';
    const builder: RequestBuilder = {
      method,
      func: request.func,
      params: request.params,
      urlParams: request.queryParams,
      model: request.model ? request.model : this.defaultModel,
      service: this.service,
    };

    const url = this.baseApi.urlBuilder(builder);
    return this.baseApi.request({ method, url, params: request.params });
  }

  getBlob(request: RequestAPI): Observable<any> {
    const method = 'GET';
    const builder: RequestBuilder = {
      method,
      func: request.func,
      params: request.params,
      urlParams: request.queryParams,
      model: request.model ? request.model : this.defaultModel,
      service: this.service,
    };

    const url = this.baseApi.urlBuilder(builder);
    return this.baseApi.requestBlob({ method, url, params: request.params });
  }

  post(request: RequestAPI): Observable<any> {
    const method = 'POST';
    const builder: RequestBuilder = {
      method,
      func: request.func,
      params: request.params,
      urlParams: request.queryParams,
      model: request.model ? request.model : this.defaultModel,
      service: this.service,
    };

    const url = this.baseApi.urlBuilder(builder);
    return this.baseApi.request({ method, url, params: request.params });
  }

  postUpload(request: RequestAPI): Observable<any> {
    const method = 'POST';
    const builder: RequestBuilder = {
      method,
      func: request.func,
      params: request.params,
      urlParams: request.queryParams,
      model: request.model ? request.model : this.defaultModel,
      service: this.service,
    };

    const url = this.baseApi.urlBuilder(builder);
    return this.baseApi.requestUpload({ method, url, params: request.params });
  }

  put(request: RequestAPI): Observable<any> {
    const method = 'PUT';
    const builder: RequestBuilder = {
      method,
      func: request.func,
      params: request.params,
      urlParams: request.queryParams,
      model: request.model ? request.model : this.defaultModel,
      service: this.service,
    };

    const url = this.baseApi.urlBuilder(builder);
    return this.baseApi.request({ method, url, params: request.params });
  }

  delete(request: RequestAPI): Observable<any> {
    const method = 'DELETE';
    const builder: RequestBuilder = {
      method,
      func: request.func,
      params: request.params,
      urlParams: request.queryParams,
      model: request.model ? request.model : this.defaultModel,
      service: this.service,
    };

    const url = this.baseApi.urlBuilder(builder);
    return this.baseApi.request({ method, url, params: request.params });
  }
}
