import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormDataUpload, RequestBlob, RequestBuilder, RequestData, RequestUpload } from '@dto/base.dto';
import { BaseUrlTypeEnum } from '@remotes/base-url/base-url.enum';
import { BaseUrlService } from '@remotes/base-url/base-url.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class BaseApiService {
  constructor(private http: HttpClient, private baseUrlService: BaseUrlService) {}

  public baseUrlBuilder(baseUrlType: any) {
    return this.baseUrlService.getBaseUrl(baseUrlType);
  }

  public urlBuilder(data: RequestBuilder): string {
    let host = this.baseUrlBuilder(BaseUrlTypeEnum.GDSERVER);
    if (data.service === 'marketplace') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.MARKETPLACE);
    } else if (data.service === 'gdtransfer') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.GDTRANSFER);
    } else if (data.service === 'payroll') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.PAYROLL);
    } else if (data.service === 'tsmarketplace') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.TSMARKETPLACE);
    } else if (data.service === 'cermati') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.TSMARKETPLACE);
    } else if (data.service === 'ppob') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.PPOB);
    } else if (data.service === 'local') {
      host = this.baseUrlBuilder(BaseUrlTypeEnum.LOCAL3000);
    }
    return `${host}/${data.model}/${data.func}?${new URLSearchParams(this.jsonToUrl(data.urlParams, false))}`;
  }

  public request<T>(data: RequestData): Observable<T> {
    const headers = new HttpHeaders({
      ...(data?.timeout && { timeout: `${data.timeout}` }),
      isNest: 'true',
      'Content-Type': 'application/json',
    });

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        Accept: 'application/json',
      }),
      body: data.params,
    };

    switch (data.method) {
      case 'GET':
        return this.http.get<T>(data.url, { headers });
      case 'POST':
        return this.http.post<T>(data.url, data.params, { headers });
      case 'DELETE':
        return this.http.delete<T>(data.url, httpOptions);
      case 'PUT':
        return this.http.put<T>(data.url, data.params, { headers });
      default:
        return this.http.get<T>(data.url);
    }
  }

  public requestBlob<T>(data: RequestBlob): Observable<HttpResponse<any>> {
    const headers = new HttpHeaders({
      ...(data?.timeout && { timeout: `${data.timeout}` }),
      isNest: 'true',
      'Content-Type': 'application/json',
    });

    switch (data.method) {
      case 'GET':
        return this.http.get<T>(data.url, { headers, observe: 'response', responseType: 'blob' as 'json' });
      case 'POST':
        return this.http.post<T>(data.url, data.params, {
          headers,
          observe: 'response',
          responseType: 'blob' as 'json',
        });
      default:
        return this.http.get<T>(data.url, { headers, observe: 'response' });
    }
  }

  public requestUpload<T>(data: RequestUpload, formDataUpload?: FormDataUpload[]): Observable<T> {
    const timeout = `${5 * 60 * 1000}`;
    const headers: HttpHeaders = new HttpHeaders({
      timeout,
      isNest: 'true',
      'Allow-Control-Allow-Origin': '*',
    });

    const formData = new FormData();
    if (formDataUpload) {
      for (const body of formDataUpload) {
        if (body.fileName) {
          formData.append(body.name, body.value, body.fileName);
        } else {
          formData.append(body.name, body.value);
        }
      }
    }

    return this.http.post<T>(data.url, formData, { headers });
  }

  public requestMethod<T>(method: string, url: string, headers: HttpHeaders, params: any): Observable<T> {
    switch (method) {
      case 'GET':
        return this.http.get<T>(url, { headers });
      case 'POST':
        return this.http.post<T>(url, params, { headers });
      default:
        return this.http.get<T>(url);
    }
  }

  private jsonToUrl(obj: any, parent: any): string {
    const parts = [];
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        parts.push(this.parseParam(key, obj[key], parent));
      }
    }
    return parts.join('&');
  }

  private parseParam(key: string, value: any, parent: string): string {
    const processedKey = parent ? parent + '[' + key + ']' : key;
    if (value && ((typeof value as string) === 'object' || Array.isArray(value))) {
      return this.jsonToUrl(value, processedKey);
    }
    return processedKey + '=' + value;
  }
}
