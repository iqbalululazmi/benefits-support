import { Injectable } from '@angular/core';
import { AuthStorageService } from '@storage/auth-storage.service';
import { CommonStorageService } from '@storage/common-storage.service';
import { environment } from 'src/environments/environment';
import { AppFromEnum } from '../token/token.constant';
import BaseUrlClass from './base-url.class';
import { BaseUrlTypeEnum } from './base-url.enum';

@Injectable({
  providedIn: 'root',
})
export class BaseUrlService {
  protected instanceAuth: any;
  constructor(private authStorageService: AuthStorageService, private commonStorageService: CommonStorageService) {}

  set auth(auth: any) {
    this.instanceAuth = auth;
  }

  get auth(): any {
    return this.instanceAuth;
  }

  async useBaseUrlService() {
    const app = await this.commonStorageService.getAppFrom();
    if (app) {
      if (app.appFrom === AppFromEnum.GREATDAY) {
        this.instanceAuth = await this.authStorageService.getSFGoAuth();
        this.instanceAuth.ist = await this.authStorageService.getStoredPrelogin();
      } else if (app.appFrom === AppFromEnum.BENEFIT) {
        this.instanceAuth = await this.authStorageService.getBenefitAuth();
      } else {
        this.instanceAuth = {};
      }
    } else {
      this.instanceAuth = {};
    }
  }

  getBaseUrl(baseUrlType: string) {
    let result: any;
    if (this.instanceAuth) {
      switch (baseUrlType) {
        case BaseUrlTypeEnum.MARKETPLACE:
          result = new BaseUrlClass(this.instanceAuth?.marketplace);
          break;
        case BaseUrlTypeEnum.GDSERVER:
          result = new BaseUrlClass(this.instanceAuth?.ist?.GOPATHNEW);
          break;
        case BaseUrlTypeEnum.GDSERVEROLD:
          result = new BaseUrlClass(this.instanceAuth?.ist?.GOPATH);
          break;
        case BaseUrlTypeEnum.TSMARKETPLACE:
          result = new BaseUrlClass(this.instanceAuth?.ist?.GOPATH + '/api');
          break;
        case BaseUrlTypeEnum.GDTRANSFER:
          result = new BaseUrlClass(this.instanceAuth?.greatdayTransfer);
          break;
        case BaseUrlTypeEnum.PPOB:
          result = new BaseUrlClass(this.instanceAuth?.greatdayPPOB);
          break;
        case BaseUrlTypeEnum.PAYROLL:
          result = new BaseUrlClass(this.instanceAuth?.payroll?.url);
          break;
        case BaseUrlTypeEnum.LOCAL3000:
          result = new BaseUrlClass('http://localhost:3000');
          break;
      }

      return result.baseUrl;
    }
  }

  getMarketplaceUrl() {
    return this.getBaseUrl(BaseUrlTypeEnum.MARKETPLACE);
  }
}
