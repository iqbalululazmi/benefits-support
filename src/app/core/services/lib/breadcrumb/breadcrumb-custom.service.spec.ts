import { TestBed } from '@angular/core/testing';

import { BreadcrumbCustomService } from './breadcrumb-custom.service';

describe('BreadcrumbCustomService', () => {
  let service: BreadcrumbCustomService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BreadcrumbCustomService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
