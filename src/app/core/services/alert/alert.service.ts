import { Injectable } from '@angular/core';
import { AlertDTO } from '@dto/alert.dto';
import { AlertController } from '@ionic/angular';
import { AlertOptions } from '@ionic/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private alertCtrl: AlertController, private translateService: TranslateService) {}

  async warning(message?: string) {
    const param = {
      cssClass: 'alert-custom',
      header: this.translateService.instant('Warning Message'),
      subHeader: this.translateService.instant(message ?? 'Warning'),
      buttons: ['OK'],
    };

    await (await this.alertCtrl.create(param)).present();
  }

  async error(message?: string, title?: string) {
    const param = {
      cssClass: 'alert-custom',
      header: this.translateService.instant(title ?? 'Error Message'),
      subHeader: this.translateService.instant(message ?? 'Error'),
      buttons: ['OK'],
    };

    await (await this.alertCtrl.create(param)).present();
  }

  async success(message?: string) {
    const param = {
      cssClass: 'alert-custom',
      header: this.translateService.instant('Success Message'),
      subHeader: this.translateService.instant(message ?? 'Successfully'),
      buttons: ['OK'],
    };

    await (await this.alertCtrl.create(param)).present();
  }

  async errorResponse(message?: any) {
    const param = {
      cssClass: 'alert-custom',
      header: this.translateService.instant('Error Message'),
      subHeader: message.error.message,
      buttons: ['OK'],
    };
    await (await this.alertCtrl.create(param)).present();
  }

  async confirm(message?: string) {
    const defaultMessage = 'Confirmation';
    return new Promise(async (resolve, reject) => {
      const param = {
        cssClass: 'alert-custom',
        header: this.translateService.instant('Confirm!'),
        message: message ? message : defaultMessage,
        buttons: [
          {
            text: this.translateService.instant('No'),
            handler: () => {
              resolve(false);
            },
          },
          {
            text: this.translateService.instant('Yes'),
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              resolve(true);
            },
          },
        ],
      };
      await (await this.alertCtrl.create(param)).present();
    });
  }

  confirmationCustom(data: AlertDTO, V2 = false) {
    return new Promise(async (resolve, reject) => {
      const param = {
        cssClass: V2 ? 'alert-confirmV2' : 'alert-confirm',
        header: data.header ? this.translateService.instant(data.header) : '',
        message: data.message ? this.translateService.instant(data.message) : '',
        buttons: [
          {
            text: this.translateService.instant(data.btnNo ?? 'No'),
            cssClass: 'btn-no',
            handler: () => {
              resolve(false);
            },
          },
          {
            text: this.translateService.instant(data.btnYes ?? 'Yes'),
            cssClass: 'btn-yes',
            handler: () => {
              resolve(true);
            },
          },
        ],
      };
      await (await this.alertCtrl.create(param)).present();
    });
  }

  async forceUpdate(message: string) {
    return new Promise(async (resolve, reject) => {
      const param: AlertOptions = {
        cssClass: 'alert-confirmV2',
        header: this.translateService.instant('Information'),
        message,
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('Update Now'),
            role: 'yes',
            cssClass: 'btn-yes',
            handler: () => {
              resolve(true);
            },
          },
        ],
      };
      await (await this.alertCtrl.create(param)).present();
    });
  }

  async loginFailed(message: string) {
    return new Promise(async (resolve, reject) => {
      const param: AlertOptions = {
        cssClass: 'alert-confirmV2',
        // header: this.translateService.instant('Information'),
        message: `
          <div class="flex-col items-center">
            <img src="assets/illustration/not-found.svg" class="w-40 m-1"></img>
            <ion-label class="font-bold text-xl">${this.translateService.instant('Login Failed')}</ion-label>
            <ion-label class="text-base">${this.translateService.instant(message)}</ion-label>
          </div>
        `,
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('Try to login again'),
            role: 'no',
            cssClass: 'btn-no',
            handler: () => {
              resolve(false);
            },
          },
          {
            text: this.translateService.instant('Open Customer Service'),
            role: 'yes',
            cssClass: 'btn-yes',
            handler: () => {
              resolve(true);
            },
          },
        ],
      };
      await (await this.alertCtrl.create(param)).present();
    });
  }

  async invalidAccessToken() {
    return new Promise(async (resolve, reject) => {
      const param: AlertOptions = {
        cssClass: 'alert-confirmV2',
        message: `
        <div class="flex-col items-center">
          <img src="assets/illustration/not-found.svg" class="w-40 m-1"></img>
          <ion-label class="font-bold text-xl">${this.translateService.instant('Your session has expired')}</ion-label>
          <ion-label class="text-base">${this.translateService.instant(
            'You will be directed to the login page to re-login',
          )}</ion-label>
        </div>
      `,
        backdropDismiss: false,
        buttons: [
          {
            text: this.translateService.instant('Go Now'),
            role: 'yes',
            cssClass: 'btn-yes',
            handler: () => {
              resolve(true);
            },
          },
        ],
      };
      await (await this.alertCtrl.create(param)).present();
    });
  }
}
