import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class SpinnerService {
  isShowSpinner = false;
  constructor(private loadingCtrl: LoadingController) {}

  async showSpinner() {
    this.isShowSpinner = true;
    this.loadingCtrl
      .create({
        spinner: 'lines',
        translucent: true,
        cssClass: 'default-spinner custom-loading',
      })
      .then((loader) => {
        loader.present().then(() => {
          if (!this.isShowSpinner) {
            loader.dismiss();
          }
        });
      });
  }

  async showSpinnerWithMessage(message?: string) {
    this.isShowSpinner = true;
    const defaultMessage = 'Please wait...';
    this.loadingCtrl
      .create({
        spinner: 'lines',
        translucent: true,
        cssClass: 'custom-class custom-loading',
        message: message ? message : defaultMessage,
      })
      .then((loader) => {
        loader.present().then(() => {
          if (!this.isShowSpinner) {
            loader.dismiss();
          }
        });
      });
  }

  async hideSpinner() {
    this.isShowSpinner = false;
    this.loadingCtrl.getTop().then((loader) => {
      if (loader) {
        loader.dismiss();
      }
    });
  }

  async isShow() {
    return this.isShowSpinner;
  }
}
