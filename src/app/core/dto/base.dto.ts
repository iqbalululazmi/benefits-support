export interface ResponseData<T> {
  data: T;
  message: string;
}

export interface ResponsePaging<T> {
  page: number;
  limit: number;
  total: number;
  totalPage: number;
  data: T[];
}

export interface RequestData {
  method: Method;
  url: string;
  params?: any;
  contentType?: string;
  timeout?: number;
  service?: string;
}

export interface RequestBuilder {
  method: Method;
  model: string;
  func?: any;
  params?: any;
  urlParams?: any;
  service?: string;
}

export interface RequestBlob {
  method: Method;
  url: string;
  params: any;
  timeout?: number;
}

export interface RequestUpload {
  method: Method;
  url: string;
  params: FormDataUpload[];
  timeout?: number;
}

export interface FormDataUpload {
  name: string;
  value: string | Blob;
  fileName?: string;
}

type Method =
  | 'GET'
  | 'HEAD'
  | 'POST'
  | 'PUT'
  | 'DELETE'
  | 'CONNECT'
  | 'OPTIONS'
  | 'PATCH';

export class RequestAPI {
  func?: string;
  params?: any = {};
  queryParams?: any = {};
  model?: any = '';
  contentType?: any = '';
  contentHeader?: any = '';
}
