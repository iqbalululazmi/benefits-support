export interface AlertDTO {
  header?: string;
  subHeader?: string;
  message?: string;
  btnYes?: string;
  btnNo?: string;
}
