export class RequestAPI {
  func: string = '';
  params?: any = {};
  queryParams?: any = {};
  model?: any = '';
  contentType?: any = '';
  contentHeader?: any = '';
}
