export interface BenefitLoginResponse {
  id: string;
  marketplace: string;
  greatdayPPOB: string;
  greatdayTransfer: string;
  isGreatdayCompany: boolean;
  ist: Ist;
  userData: BenefitUserData;
}

export interface Ist {
  GOPATH: string;
  GOPATHNEW: string;
}

export interface BenefitUserData {
  accountName: string;
  companyCode: string;
  companyId: number;
  companyName: string;
  userId: number;
  username: string;
  empId: string;
  empNo: string;
  fullname: string;
  photo: string;
  position: string;
  positionId: any;
  email: string;
  phone: string;
  address: string;
  joinDate: string;
  endDate: any;
  isSF6: number;
  isTNC: boolean;
  department: string;
  gender: number;
  birthPlace: string;
  birthdate: string;
  empMaritalStatus: number;
  identityNo: string;
  nationalityCode: string;
  employmentStatus: string;
  bankCode: string;
  bankAccountNo: string;
  bankAccountName: string;
}
