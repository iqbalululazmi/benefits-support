import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { StoragePreferenceEnum, StoragePreferenceSpecDTO } from './constant/storage.constant';

@Injectable({
  providedIn: 'root',
})
export class BaseStorageService {
  private static getStorageSpec(spec: StoragePreferenceEnum): StoragePreferenceSpecDTO {
    switch (spec) {
      case StoragePreferenceEnum.Auth:
        return { key: 'Auth' };
      case StoragePreferenceEnum.Saas:
        return { key: 'SFGoStorage' };
      case StoragePreferenceEnum.CompanySetting:
        return { key: 'GDAttendanceSetting' };
      case StoragePreferenceEnum.TokenNotification:
        return { key: 'fcmToken' };
      case StoragePreferenceEnum.Language:
        return { key: 'language' };

      case StoragePreferenceEnum.SFGoAuth:
        return { key: 'SFGoAuth' };
      case StoragePreferenceEnum.BenefitAuth:
        return { key: 'BenefitAuth' };
      case StoragePreferenceEnum.APPFrom:
        return { key: 'APPFrom' };
      case StoragePreferenceEnum.StorePrelogin:
        return { key: 'StorePrelogin' };
      case StoragePreferenceEnum.FilterEmpPayrollData:
        return { key: 'Filter-Emp-Payroll-Data' };

      // Greatday Transfer
      case StoragePreferenceEnum.GDTransferAuth:
        return { key: 'GDTransferAuth' };

      // Topup and bill last transaction
      case StoragePreferenceEnum.TBLastTransaction:
        return { key: 'TBLastTransaction' };
    }
  }

  async getGreatDayStorage<T>(spec: StoragePreferenceEnum): Promise<T> {
    const storageSpect = BaseStorageService.getStorageSpec(spec);
    const { value } = await Storage.get({ key: storageSpect.key });
    const result = value ? JSON.parse(value) : '';
    return result as T;
  }

  async setGreatDayStorage(spec: StoragePreferenceEnum, value: any) {
    await Storage.set({
      key: BaseStorageService.getStorageSpec(spec).key,
      value: JSON.stringify(value),
    });
  }

  async removeGreatDayStorage(spec: StoragePreferenceEnum) {
    await Storage.remove({
      key: BaseStorageService.getStorageSpec(spec).key,
    });
  }

  async clearGreatDayStorage() {
    await Storage.clear();
  }
}
