import { Injectable } from '@angular/core';
import { BaseStorageService } from '@storage/base-storage.service';
import { StoragePreferenceEnum } from './constant/storage.constant';

@Injectable({
  providedIn: 'root',
})
export class CommonStorageService {
  constructor(private baseStorageService: BaseStorageService) {}

  async getAppFrom() {
    return await this.baseStorageService.getGreatDayStorage<{ appFrom: string; userAgent: any }>(
      StoragePreferenceEnum.APPFrom,
    );
  }

  async setAppFrom(value: { appFrom: string; userAgent: any }) {
    return await this.baseStorageService.setGreatDayStorage(StoragePreferenceEnum.APPFrom, value);
  }

  async getlastTransaction<T>(): Promise<T> {
    return (await this.baseStorageService.getGreatDayStorage(StoragePreferenceEnum.TBLastTransaction)) as T;
  }

  async setLastTransaction(value: any) {
    return this.baseStorageService.setGreatDayStorage(StoragePreferenceEnum.TBLastTransaction, value);
  }

  async removeLastTransaction() {
    return this.baseStorageService.removeGreatDayStorage(StoragePreferenceEnum.TBLastTransaction);
  }
}
