import { Injectable } from '@angular/core';
import { BenefitLoginResponse } from '@dto/auth-benefit.dto';
import { GDTransferAuthResponse, LoginResponse, SaasResponse, StoredPrelogin } from '@dto/auth.dto';
import { AppFromEnum } from '@remotes/token/token.constant';
import { BaseStorageService } from '@storage/base-storage.service';
import { CommonStorageService } from './common-storage.service';
import { StoragePreferenceEnum } from './constant/storage.constant';

@Injectable({
  providedIn: 'root',
})
export class AuthStorageService {
  constructor(private baseStorageService: BaseStorageService, private commonStorageService: CommonStorageService) {}

  async getAuth() {
    const app = await this.commonStorageService.getAppFrom();
    if (app) {
      if (app.appFrom === AppFromEnum.GREATDAY) {
        return await this.getSFGoAuth();
      } else if (app.appFrom === AppFromEnum.BENEFIT) {
        return await this.getBenefitAuth();
      } else {
        return {};
      }
    } else {
      return {};
    }
  }

  async getSFGoAuth() {
    return await this.baseStorageService.getGreatDayStorage<LoginResponse>(StoragePreferenceEnum.SFGoAuth);
  }

  async getBenefitAuth() {
    return await this.baseStorageService.getGreatDayStorage<BenefitLoginResponse>(StoragePreferenceEnum.BenefitAuth);
  }

  async setSFGoAuth(auth: LoginResponse) {
    await this.baseStorageService.setGreatDayStorage(StoragePreferenceEnum.SFGoAuth, auth);
  }

  async setBenefitAuth(auth: BenefitLoginResponse) {
    await this.baseStorageService.setGreatDayStorage(StoragePreferenceEnum.BenefitAuth, auth);
  }

  async getStoredPrelogin() {
    return await this.baseStorageService.getGreatDayStorage<StoredPrelogin>(StoragePreferenceEnum.StorePrelogin);
  }

  async setStoredPrelogin(storePrelogin: StoredPrelogin) {
    await this.baseStorageService.setGreatDayStorage(StoragePreferenceEnum.StorePrelogin, storePrelogin);
  }

  async getSaas() {
    return await this.baseStorageService.getGreatDayStorage<SaasResponse>(StoragePreferenceEnum.Saas);
  }

  async getAuthGdTransfer() {
    return await this.baseStorageService.getGreatDayStorage<GDTransferAuthResponse>(
      StoragePreferenceEnum.GDTransferAuth,
    );
  }

  async setAuthGdTransfer(auth: GDTransferAuthResponse) {
    await this.baseStorageService.setGreatDayStorage(StoragePreferenceEnum.GDTransferAuth, auth);
  }
}
