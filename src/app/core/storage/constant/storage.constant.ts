export interface StoragePreferenceSpecDTO {
  key: string;
}

export enum StoragePreferenceEnum {
  Auth,
  Saas,
  CompanySetting,
  TokenNotification,
  Language,
  StorePrelogin,

  // Payroll
  SFGoAuth,
  FilterEmpPayrollData,
  BenefitAuth,
  APPFrom,

  // Greatday Transfer
  GDTransferAuth,

  // Topup and Bill last transaction
  TBLastTransaction,
}

export class CapacitorStorage {
  static readonly CapStorage = 'CapacitorStorage';
  static readonly APPFrom = CapacitorStorage.CapStorage + '.APPFrom';
  static readonly SFGoAuth = CapacitorStorage.CapStorage + '.SFGoAuth';
  static readonly BenefitAuth = CapacitorStorage.CapStorage + '.BenefitAuth';

  // private to disallow creating other instances of this type
  private constructor(public readonly value: any) {}
}
