import { BaseMenu } from '../../models/menu.model';
import { RouteEnum } from '../routes/routes';

export const menuBenefit: BaseMenu[] = [
  {
    megaMenu: 'Dashboard',
    subMenu: 'Dashboard',
    sideType: 'activity',
    activeSubMenu: false,
    route: RouteEnum.HOME,
  },
  {
    megaMenu: 'Benefit Setting',
    subMenu: 'Benefit Setting',
    sideType: 'management',
    activeSubMenu: false,
    route: RouteEnum.BENEFIT_SETTING,
  },
  {
    megaMenu: 'Internal Plan',
    subMenu: 'Internal Plan',
    sideType: 'management',
    activeSubMenu: false,
    route: RouteEnum.INTERNAL_PLAN,
  },
  {
    megaMenu: 'Block User',
    subMenu: 'Block User',
    sideType: 'management',
    activeSubMenu: false,
    route: RouteEnum.BLOCK_USER,
  },
  {
    megaMenu: 'Data Synchronization',
    subMenu: 'Data Synchronization',
    sideType: 'management',
    activeSubMenu: false,
    route: RouteEnum.DATA_SYNC,
  },
  {
    megaMenu: 'Billing',
    subMenu: 'Billing',
    sideType: 'report',
    activeSubMenu: false,
    route: RouteEnum.BILLING,
  },
  {
    megaMenu: 'Transaction History',
    subMenu: 'Transaction History',
    sideType: 'report',
    activeSubMenu: false,
    route: RouteEnum.TRANSACTION_REPORT,
  },
  {
    megaMenu: 'Team Member',
    subMenu: 'Team Member',
    sideType: 'administrator',
    activeSubMenu: false,
    route: RouteEnum.TEAM_MEMBER,
  },
];
