export interface BaseMenu {
  id?: number;
  megaMenu: string;
  subMenu: string;
  sideType: string;
  activeSubMenu: boolean;
  route: string;
}

export interface NavBar {
  menu: string;
  key: string;
}

export interface PopOver {
  key: string;
  menuGroup: MenuGroup[];
}

export interface MenuGroup {
  key: string;
  title: string;
  auth: string;
  submenu: BaseMenu[];
}
